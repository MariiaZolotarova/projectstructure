﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Context.Entity;

namespace ProjectStructure.BL.Context
{
    public class ProjectContext : DbContext
    {
        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options) { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany(t => t.Projects)
                .HasForeignKey(p => p.AuthorId)
                .HasPrincipalKey(t => t.Id);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Projects)
                .HasForeignKey(p => p.TeamId)
                .HasPrincipalKey(t => t.Id);

            modelBuilder.Entity<Task>()
                .HasOne(p => p.Project)
                .WithMany(t => t.Tasks)
                .HasForeignKey(p => p.ProjectId)
                .HasPrincipalKey(t => t.Id);

            modelBuilder.Entity<Task>()
                .HasOne(p => p.Performer)
                .WithMany(t => t.Tasks)
                .HasForeignKey(p => p.PerformerId)
                .HasPrincipalKey(t => t.Id);

            modelBuilder.Entity<User>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Users)
                .HasForeignKey(p => p.TeamId)
                .HasPrincipalKey(t => t.Id);
        }
    }
}