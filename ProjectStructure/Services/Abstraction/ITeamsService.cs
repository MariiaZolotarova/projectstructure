﻿using System.Collections.Generic;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;

namespace ProjectStructure.Services.Abstraction
{
    public interface ITeamsService
    {
        List<Team> GetTeams();
        Team GetTeamId(int id);
        void CreateTeam(TeamDto teamDto);
        void DeleteTeam(int id);
        void UpdateTeam(int id, TeamDto teamDto);
    }
}
