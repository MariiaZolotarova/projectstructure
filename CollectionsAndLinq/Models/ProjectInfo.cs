﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinq.Models
{
    public class ProjectInfo
    {
        public ProjectModel Project { get; set; }
        public TaskModel LongestTask { get; set; }
        public TaskModel ShortestTask { get; set; }
        public int UsersCount { get; set; }
    }
}
